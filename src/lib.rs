#![allow(non_snake_case)]

use godot::engine::{Node, AudioStreamGeneratorPlayback, AudioStreamPlayer3D, InputEvent, InputEventKey};
use godot::prelude::*;
use soundGen::SoundGenerator;

use std::collections::HashMap;
// use std::any::Any;
// use godot::sys::GodotFfi;
use std::fs::File;
use std::ops::Add;
// use std::ops::DerefMut;
use std::sync::Arc;

use rustysynth::*;



mod soundGen;
mod midiPlayer;
// mod midiTaker;

#[allow(unused_variables)]
#[derive(GodotClass)]
#[class(base=Node)]
pub struct Ensemble {
    pub pan: i32,
    pub vol: i32,
    pub soundGenerators: HashMap<String,(u8,u8,SoundGenerator)>,
    pub player : Option<Gd<AudioStreamPlayer3D>>,
    pub playback : Option<Gd<AudioStreamGeneratorPlayback>>,
    // #[export]
    // pub channelRestrictions: PackedByteArray,
    //Todo : bro
    #[base]
    base: Base<Node>,
}

#[godot_api]
impl Ensemble {

  
  #[func]
  fn noteOn(&mut self, channel: u8, key:i32, velocity: i32)  {
    self.soundGenerators.iter_mut().for_each(|(_,(s,e,sGen))|{
      if *s<= channel && *e > channel{
        sGen.noteOn(channel, key, velocity);
      }
    });
  }
  #[func]
  fn sendCC(&mut self, channel: u8, controller:u8, value: u8)  {
    self.soundGenerators.iter_mut().for_each(|(_,(s,e,sGen))|{
      if *s<= channel && *e > channel{
        sGen.sendCC(channel, controller, value);
      }
    });
  }
  #[func]
  fn noteOff(&mut self, channel: u8, key:i32)  {
    self.soundGenerators.iter_mut().for_each(|(_,(s,e,sGen))|{
      if *s<= channel && *e > channel{
        sGen.noteOff(channel, key );
      }
    });
  }

  #[func]
  fn notesOff(&mut self, channel: u8)  {
    self.soundGenerators.iter_mut().for_each(|(_,(s,e,sGen))|{
      if *s<= channel && *e > channel{
        sGen.notesOff(channel );
      }
    });
  }


  #[func]
  fn allNotesOff(&mut self)  {
    self.soundGenerators.iter_mut().for_each(|(_,(_s,_e,sGen))|{
        sGen.allNotesOff( );
    });
  }

  #[func]
  fn pitchBend(&mut self, channel: u8, msb:u8, lsb:u8)  {
    self.soundGenerators.iter_mut().for_each(|(_,(s,e,sGen))|{
      if *s<= channel && *e > channel{
        sGen.pitchBend(channel, msb,lsb );
      }
    });
  }


  #[func]
  fn setInstrument(&mut self, channel:u8, ins: u8){
    self.soundGenerators.iter_mut().for_each(|(_,(s,e,sGen))|{
      if *s<= channel && *e > channel{
        sGen.setInstrument(channel,ins);
      }
    });
  }

  #[func]
  fn render_separate(&mut self,playbacks: VariantArray) {
    (0..playbacks.len()).for_each(|i|{
      let args : VariantArray= playbacks.get(i).to();
      let name : StringName= args.get(0).to();
      let mut playback : Gd<AudioStreamGeneratorPlayback>= args.get(1).to();
      let totalframes = playback.get_frames_available() as usize;
      self.soundGenerators.get_mut(&name.to_string()).map(|(_,_, ref mut sGen)|{
        let (l,r) = sGen.render(totalframes);
        let vecs:PackedVector2Array   = (0..totalframes).map(|j|{
            Vector2 {x:l[j], y:r[j]}
          }).collect();
        playback.push_buffer(vecs);
      });
    });
  }


  
  #[func]
  fn loadSoundfont(&mut self ,name: GodotString, start:u8, end:u8, soundFont: GodotString){
    let sg_try= SoundGenerator::newRSynth(&soundFont.to_string());
    match sg_try {
      Ok(sg) => { self.soundGenerators.insert(name.into(),(start,end,sg));}
      Err(err) => {
        godot_error!("unable to load soundfont file ");
      }
    }
  }
  
  #[func]
  fn loadCSynth(&mut self,name: GodotString, start:u8, end:u8, csw: u8){
    self.soundGenerators.insert(name.into(),(start,end,SoundGenerator::newCSynth(csw)));
  }
  
  // #[func]
  // fn testPlayMidiFile(&mut self, path : GodotString) -> PackedVector2Array {

    //   // Load the MIDI file.
  //   let mut mid = File::open(path.to_string()).unwrap();
  //   let midi_file = Arc::new(MidiFile::new(&mut mid).unwrap());
  //   godot_print!("{} loaded successfully", path);
  
  //   // Play the MIDI file.
  //   self.sf_sequencer.play(&midi_file, false);

  //   // The output buffer.
  //   let sample_count = (self.sf_settings.sample_rate as f64 * midi_file.get_length()) as usize;
  //   let mut left: Vec<f32> = vec![0_f32; sample_count];
  //   let mut right: Vec<f32> = vec![0_f32; sample_count];
  
  //   // Render the waveform.
  //   self.sf_sequencer.render(&mut left[..], &mut right[..]);

  //   let arr :  PackedVector2Array = left.iter().enumerate().map(|(i,left_frame)| {
  //     Vector2::new(*left_frame, *right.get(i).unwrap())
  //   }).collect()

  //   return arr;

  // }

}

#[godot_api]
impl NodeVirtual for Ensemble {
  fn init(base: Base<Node>) -> Self {
    Ensemble {
        pan: 50,
        vol: 107,
        soundGenerators:HashMap::new(),
        player:None,
        playback:None,
        base,
    }
  }
  fn ready(&mut self) {
    godot_print!("Musician node successfully initialized. \n Don't forget to load a sound generator.");
  }   

  fn process (&mut self, delta:f64){
    self.soundGenerators.iter_mut().for_each(|(_,(_,_,sg))|{sg.process(delta)});
  }

  // if $player.playing:
  // var playback :AudioStreamGeneratorPlayback = $player.get_stream_playback()
  // if event is InputEventKey :
  //   if event.pressed and not event.echo:
  //     $Musician.noteOn(event.keycode,127,playback)
  //   elif not event.pressed:
  //     $Musician.noteOff(event.keycode,127,playback)
  
}

struct Terpsikore;

#[gdextension]
unsafe impl ExtensionLibrary for Terpsikore {}