#![allow(non_snake_case)]

use dashmap::DashMap;
use godot::engine::{Node, AudioStreamGeneratorPlayback, AudioStreamPlayer3D, InputEvent, InputEventKey};
use godot::prelude::*;
use midir::{MidiInput, Ignore};
use midly::live::LiveEvent;
use midly::{Smf, Format, MidiMessage};
use nodi::{Sheet, Player, Connection, MidiEvent};
use nodi::timers::{Ticker, ControlTicker};
use midly::MidiMessage::*;
use std::error::Error;
use std::thread::{self, JoinHandle};

// use std::any::Any;
// use godot::sys::GodotFfi;
use std::fs::{File, self};
// use std::ops::DerefMut;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::*;

use crate::soundGen::*;

use std::path::{self, Path};

type ChannelRange = (u8,u8);


#[allow(unused_variables)]
#[derive(GodotClass)]
#[class(base=Node)]
pub struct MidiTaker{
  pub input : Option<MidiInput>,
  pub active: bool,
  #[base] 
  pub base: Base<Node>
}

#[godot_api]
impl NodeVirtual for MidiTaker {
  fn init(base: Base<Node>) -> Self {
    Self {
      input:None,
      active:false,
      base
    }
  }
  
}
#[godot_api]
impl MidiTaker {
  fn startConnection(&mut self, noteOn: Callable, noteOff :Callable, pcChange: Callable){
    self.input.map(|midi_in|{
      let in_port_name = midi_in.port_name(in_port)?;
      // Get an input port (read from console if multiple are available)
      let in_ports = midi_in.ports();
      let in_port = match in_ports.len() {
          0 => {godot_error!("port not found");return;}
          _ => {
              &in_ports[0]
          }
      };
      // _conn_in needs to be a named parameter, because it needs to be kept alive until the end of the scope
      let _conn_in = midi_in.connect(
          in_port,
          "midir-read-input",
          move |stamp, msg, _| {
            let event = LiveEvent::parse(msg).unwrap();
            match event {
              LiveEvent::Midi { channel, message } => match message {
                MidiMessage::NoteOn { key, vel } => {
                  noteOn.callv(Array::from_iter(
                    [Variant::from(key.as_int()),Variant::from(vel.as_int())]
                  ));
                }
                MidiMessage::NoteOff { key, vel } => {
                  noteOff.callv(Array::from_iter(
                    [Variant::from(key.as_int()),Variant::from(vel.as_int())]
                  ));
                }
                _ => {}
              }
              _ => {}
            }
          }
        ,());
        });
  }


  #[func]
  fn setupPort(&mut self) -> bool {
    match self._start() {
      Ok (()) => {
        true
      }
      _ => {
        godot_print!("coulnd't establish midi in connection !");
        false
      }
    }
  }


  fn _setupPort(&mut self) -> Result<(), Box <dyn Error>>{
    let mut input = String::new();

    let mut midi_in = MidiInput::new("terpsikore in")?;
    midi_in.ignore(Ignore::None);

    
    self.input=Some(midi_in);
  }
  
}