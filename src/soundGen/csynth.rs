#![allow(non_snake_case)]

use godot::engine::{Node, AudioStreamGeneratorPlayback, AudioStreamPlayer3D, InputEvent, InputEventKey};
use godot::prelude::*;
use rustysynth::{Synthesizer, SynthesizerSettings, SoundFont};

use std::f64::consts::{PI, TAU};



#[derive(Debug)]
#[derive(Clone, Copy)]
pub enum CSW {
    Sin,
    Tri,
    Squ,
    Custom(u8)
}

impl From<u8> for CSW {
    fn from(ins:u8) -> Self {
        match ins {
            0 => {Self::Sin}
            1 => {Self::Tri}
            2 => {Self::Squ}
            _x => {Self::Custom(ins-3)}
        }
    }
}

pub fn twinSilence(_phi:f64) -> (f32,f32){
    (0.,0.)
}

pub fn twin(func: fn(f64) -> f64) -> impl Fn(f64) -> (f32,f32) {
    move |x|{
        (func(x) as f32,func(x) as f32)
    }
}


pub fn tri (phi:f64) -> f64{
   (2./PI)*(phi).sin().asin()
}

pub fn square(phi:f64) -> f64 {
    if phi > PI {
        return 1.
    } else {
        return 0.
    }
} 
pub struct VoiceProcess {
    pub current_time : f64,
    // TIme -> amplitude(left, right)
    pub function : fn(f64) -> (f32,f32), 
    pub value : f64,
    pub volume : f32,
    pub note_info : Option<i32>,
    pub phase: f64,
    pub sample_rate : f64
}
 

impl VoiceProcess {
    pub fn new(wave_type:CSW, spf : Option<fn(f64) -> (f32,f32)>) -> Self {
        let function: fn (f64) ->(f32,f32) = match wave_type {
            CSW::Sin    => {|x| {twin(f64::sin)(x)}}  
            CSW::Tri    => {|x| {twin(tri)(x) }}
            CSW::Squ    => {|x| {twin(square)(x)}} 
            _           => {spf.unwrap_or(twinSilence)}
        };
        Self { 
            current_time:0.,
            function,
            value:0.,
            volume:1.0,
            note_info:None,
            phase:0.,
            sample_rate: 48000. 
        }
    }

    pub fn process(&mut self, delta:f64){
        self.current_time+=delta;
    }

    pub fn render(&mut self, left: &mut [f32], right: &mut [f32]) {
        if self.function == twinSilence {
            return ;
        }
        self.note_info.map(|note| {
            let freq = 440.0*(2_f64).powf((note as f64-69.)/12.);
            let increment = freq/self.sample_rate;
            let length = left.len();
            (0..length).for_each(|i| {
                self.phase=  increment + self.phase.fposmod(1.0);
                let (l,r) =(self.function)(self.phase * TAU);
                left[i] +=  l;
                right[i]+= r;
            });
        });
    }   
}

pub struct CSynth {
    pub voices : [CSW;16],
    pub vProcesses: [VoiceProcess; 16],
    pub channel : u8,
}

impl CSynth {
    pub fn new(csw :u8) -> Self {
        let voices=  core::array::from_fn(|_|{CSW::from(csw)});
        Self {
            voices,
            vProcesses: core::array::from_fn(|i| {VoiceProcess::new(voices[i],None)}),
            channel:0,
        }
    }

    pub fn process(&mut self, delta:f64){
        self.vProcesses.iter_mut().for_each(|vP|{
            vP.process(delta);
        })
    }


    pub fn setInstrument(&mut self, channel: u8, ins: u8) -> Result<(),&str>{
        if ins > 16 {
            return Err("instrument number over the limit !");
        }
        return Ok(self.voices[channel  as usize % 16 ] = CSW::from(ins));
    }

// u8 -> 256

  pub fn render(&mut self, left:&mut [f32], right:&mut [f32]) {
    if left.len() != right.len() {
        panic!("The output buffers for the left and right must be the same length.");
    }
    self.vProcesses.iter_mut().for_each(|vP|{
        vP.render(left, right);
    });
  }

}