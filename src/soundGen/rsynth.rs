#![allow(non_snake_case)]

use godot::engine::{Node, AudioStreamGeneratorPlayback, AudioStreamPlayer3D, InputEvent, InputEventKey};
use godot::prelude::*;
use rustysynth::{Synthesizer, SynthesizerSettings, SoundFont};

// use godot::sys::GodotFfi;
use std::fs::File;
// use std::ops::DerefMut;
use std::sync::Arc;

use std::io::Error;


pub struct RSynth {
    pub soundfont : Arc<SoundFont>,
    pub sf_settings : SynthesizerSettings,
    pub sf_synthesizer : Synthesizer,
}

impl RSynth {
  pub fn new(sf_name: &str) ->Result<Self,Error> {
      let sf2_try = File::open(sf_name);
      sf2_try.map(|mut sf2|{
        let soundfont: Arc<SoundFont> = Arc::new(SoundFont::new(&mut sf2).unwrap());
    
        // Create the synthesizer. 
        let sf_settings = SynthesizerSettings::new(48000);
        let sf_synthesizer = Synthesizer::new(&soundfont, &sf_settings).unwrap();
        
        godot_print!("RustySynth successfully created with the soundfont {}", sf_name);

        Self {
            soundfont,
            sf_settings,
            sf_synthesizer
        }
      })
  }

  pub fn setInstrument(&mut self, channel:u8, ins: u8){
    self.sf_synthesizer.process_midi_message(channel as i32, 0xC0, ins as i32, 0);
  }

}