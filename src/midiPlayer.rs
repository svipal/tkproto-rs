#![allow(non_snake_case)]

use dashmap::DashMap;
use godot::engine::{Node, AudioStreamGeneratorPlayback, AudioStreamPlayer3D, InputEvent, InputEventKey};
use godot::prelude::*;
use midly::{Smf, Format};
use nodi::{Sheet, Player, Connection, MidiEvent};
use nodi::timers::{Ticker, ControlTicker};
use midly::MidiMessage::*;
use std::thread::{self, JoinHandle};

// use std::any::Any;
// use godot::sys::GodotFfi;
use std::fs::{File, self};
// use std::ops::DerefMut;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::*;

use crate::soundGen::*;

use std::path::{self, Path};

type ChannelRange = (u8,u8);


#[allow(unused_variables)]
#[derive(GodotClass)]
#[class(base=Node)]
pub struct MidiPlayer{
  pub filename:GodotString,
  #[export]
  
  pub sPlayer: Gd<SignalPlayer>,
  pub pauser:Option<Sender<()>>,
  pub ender:Option<Sender<()>>,
  // the receiver is taken by the playing thread
  pub pauseReceiver : Option<Receiver<()>>,
  pub endReceiver : Option<Receiver<()>>,
  pub pause_indic: bool,
  pub play_indic: bool,
  pub data : Vec<u8>,
  #[base] 
  pub base: Base<Node>

}

#[godot_api]
impl MidiPlayer{

  #[signal]
  fn playbackStarted();
  #[signal]
  fn playbackEnded();

  #[func]
  fn playing(&mut self,) -> bool {
    return self.play_indic
  }


  #[func]
  fn getPauser(&mut self) -> Option<Gd<GdSender>>{
    match self.pauser.as_ref(){
      Some(_) => Some(Gd::new(GdSender{sender:self.pauser.take()})),
      None => None
    }
  }
  #[func]
  fn getEnder(&mut self) -> Option<Gd<GdSender>>{
    match self.ender.as_ref(){
      Some(_) => Some(Gd::new(GdSender{sender:self.ender.take()})),
      None => None
    }
  }
  #[func]
  fn setCallbacks(&mut self, note_on: Callable,note_off: Callable,pitch_bend: Callable,send_cc: Callable,pc_change: Callable,){
   
    
    self.sPlayer.connect("note_on".into(), note_on);
    self.sPlayer.connect("note_off".into(), note_off);
    self.sPlayer.connect("pitch_bend".into(), pitch_bend);
    self.sPlayer.connect("send_cc".into(), send_cc);
    self.sPlayer.connect("pc_change".into(), pc_change);
  }

  #[func]
  fn prepareFilePlayback(&mut self,filename:GodotString){
    let (pauser,receiver) = channel();
    self.pauser = Some(pauser);
    self.pauseReceiver = Some(receiver);
    let (ender,endReceiver) = channel();
    self.ender = Some(ender);
    self.endReceiver= Some(endReceiver);

    match fs::read(&filename.to_string()) {
      Ok(data) => {
        self.filename = filename;
        self.data=data
      }
      Err(err) => {
        godot_error!("couldn't create player node")
      }
    }
  }

  #[func]
  fn startPlayback(&mut self){
    if self.pauseReceiver.is_none() || self.data.is_empty() {
      godot_error!("Set up a player first !");
      return;
    }
    if self.play_indic == true {
      godot_error!("You are already playing back a midi file using this node");
      return;
    }
    
    
    
    self.pause_indic=false;
    
    let preceiver = self.pauseReceiver.take();
    let ereceiver = self.endReceiver.take().unwrap();
    let binding = Smf::parse(&self.data);
    match binding {
      Ok(binding) => {
        let Smf { header,tracks } = binding;
        let timer = Ticker::try_from(header.timing).unwrap().to_control(preceiver.unwrap());
        self.pauseReceiver = None;
        let mut player = Player::new(timer, RefSp {rs:&mut self.sPlayer,earlystop:&ereceiver});
        let sheet = match header.format {
          Format::SingleTrack | Format::Sequential => Sheet::sequential(&tracks),
          Format::Parallel => Sheet::parallel(&tracks),
        };
        godot_print!("midi parse successful, starting playback");
        self.base.emit_signal("playbackStarted".into(), &[]);
        player.play(&sheet);
        self.play_indic = true;
        self.pauseReceiver = None;
        self.ender = None;
        self.pauser=None;
        godot_print!("ending playback : all pausers & enders are now invalid..");
        self.base.emit_signal("playbackEnded".into(), &[]);
        self.play_indic = false;
      }
      _ => {
        godot_print!("couldn't parse midi data...");
        return;
      }

    }
    
  }
}

#[allow(unused_variables)]
#[derive(GodotClass)]
#[class(base=RefCounted)]
pub struct GdSender {
  pub sender: Option<Sender<()>>,
}

#[godot_api]
impl GdSender {
  fn setup(&mut self,pauser: Sender<()>){
    self.sender=Some(pauser);
  }
  #[func]
  fn send(&self) {
    self.sender.as_ref().map(|x|{x.send(())});
  }
}


#[godot_api]
impl NodeVirtual for MidiPlayer {
  fn init(base:Base<Node>) -> Self{
    Self {
      sPlayer:Gd::<SignalPlayer>::new_default(),
      filename:"".into(),
      data:vec![],
      pause_indic:false,
      play_indic:false,
      pauser:None,
      ender:None,
      endReceiver:None,
      pauseReceiver:None,
      base
    }
  }
}

#[allow(unused_variables)]
#[derive(GodotClass)]
#[class(base=Node)]
pub struct SignalPlayer {
  #[base]
  base:Base<Node>
}

unsafe impl Sync for SignalPlayer {}
unsafe impl Send for SignalPlayer {}

pub struct RefSp<'a> {
  rs:&'a mut Gd<SignalPlayer>,
  earlystop:&'a Receiver<()>
}


#[godot_api]
impl NodeVirtual for SignalPlayer {
  fn init(base:Base<Node>) -> Self{
    Self {
      base
    }
  }
}

#[godot_api]
impl SignalPlayer {
  #[signal]
  fn note_on(channel:u8,key:u8,velocity:u8);
  #[signal]
  fn note_off(channel:u8,key:u8,velocity:u8);
  #[signal]
  fn pc_change(channel:u8,program:u8);
  #[signal]
  fn pitch_bend(channel:u8,msb:u8,lsb:u8);
  #[signal]
  fn send_cc(channel:u8,controller:u8,value:u8);

}


impl Connection for RefSp<'_> {
  fn play(&mut self, event: MidiEvent) -> bool {
    match self.earlystop.try_recv() {
      Err(_) => {}
      _ =>{return false}
    }
    
    match event.message {
      NoteOff { key, vel } => {
        self.rs.emit_signal("note_off".into(), &[Variant::from(event.channel.as_int()),Variant::from(key.as_int()),Variant::from(vel.as_int())]);
      }
      NoteOn { key, vel } => {
        self.rs.emit_signal("note_on".into(), &[Variant::from(event.channel.as_int()),Variant::from(key.as_int()),Variant::from(vel.as_int())]);
      },
      Aftertouch { key, vel } => {
      
      },
      Controller { controller, value } => {   
        self.rs.emit_signal("send_cc".into(),&[Variant::from(event.channel.as_int()),Variant::from(controller.as_int()),Variant::from(value.as_int())]);
      }
      ProgramChange { program } => {
        self.rs.emit_signal("pc_change".into(), &[Variant::from(event.channel.as_int()),Variant::from(program.as_int())]);
      
      },
      ChannelAftertouch { vel } => {},
      PitchBend { bend } => {
        let (msb,lsb) = unu14(bend);
        self.rs.emit_signal("pitch_bend".into(), &[Variant::from(event.channel.as_int()),Variant::from(lsb),Variant::from(msb)]);
      },
    }
        
  true
  }
}

fn unu14(b:midly::PitchBend)-> (u8,u8){
  let raw = b.0.as_int();
  return ((raw >> 7) as u8,(raw & 0x7F) as u8)
}