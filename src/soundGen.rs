#![allow(non_snake_case)]

use godot::engine::{Node, AudioStreamGeneratorPlayback, AudioStreamPlayer3D, InputEvent, InputEventKey};
use godot::prelude::*;


// use godot::sys::GodotFfi;

use rustysynth::*;

mod rsynth;
mod csynth;

use self::rsynth::RSynth;
use self::csynth::CSynth;

use std::io::Error;

// used for individual instruments 

pub enum SoundGenerator {
    RG(RSynth),
    CG(CSynth),
    Uninitialized
}

impl SoundGenerator{
    pub fn newProteus() -> Result<Self,Error> {
        Self::newRSynth("proteus.sf2")
    } 

    pub fn newRSynth(sf_name : &str)-> Result<Self,Error>  {
        match RSynth::new(sf_name) {
            Ok (rg) => {Ok(Self::RG(rg))}
            Err(err) => Err(err)
        }
    }

    pub fn process(&mut self, delta:f64) {
        match self {
            Self::CG(cs) => {
                cs.process(delta);
            } 
            _ => {}
        }
    }

    pub fn newCSynth(csw : u8) -> Self{
        return Self::CG(CSynth::new(csw));
    }

    pub fn render(&mut self, framecount:usize) -> (Vec<f32>,Vec<f32>) {
        match self {
            Self::RG(rs) => {
        
                let mut left: Vec<f32> = vec![0_f32; framecount];
                let mut right: Vec<f32> = vec![0_f32; framecount];
            
                // Render the waveform.
                rs.sf_synthesizer.render(&mut left[..], &mut right[..]);
                (left,right)
            } 
            Self::CG(cs) => {
                let mut left: Vec<f32> = vec![0_f32; framecount];
                let mut right: Vec<f32> = vec![0_f32; framecount];
                // godot_print!("rendering {} frames", framecount);
                cs.render(&mut left, &mut right);
                (left,right)
            }
            _ => {
                let left: Vec<f32> = vec![0_f32; framecount];
                let right: Vec<f32> = vec![0_f32; framecount];
                (left,right)
            }
        }
    }

    pub fn noteOn(&mut self, channel: u8,key:i32, velocity: i32)  {
        match self {
            Self::RG(rs) => {
                rs.sf_synthesizer.note_on(channel as i32, key, velocity);
            }
            Self::CG(cs) => {
                cs.vProcesses[channel as usize % 16].note_info = Some(key);
                cs.vProcesses[channel as usize % 16].phase = 0.;
                cs.vProcesses[channel as usize % 16].volume = velocity as f32/127.;
            }
            _ => {}
        }
    }
    pub fn noteOff(&mut self, channel: u8, key:i32)  {
        match self {
            Self::RG(rs) => {
                rs.sf_synthesizer.note_off(channel as i32, key );
            }
            Self::CG(cs) => {
                cs.vProcesses[channel as usize % 16].note_info = None;
                cs.vProcesses[channel as usize % 16].current_time = 0.;
            }
            _ => {}
        }
    }

    pub fn notesOff(&mut self, channel: u8)  {
        match self {
            Self::RG(rs) => {
                rs.sf_synthesizer.note_off_all_channel(channel as i32, false);
            }
            Self::CG(cs) => {
               
            }
            _ => {}
        }
    }
    pub fn allNotesOff(&mut self)  {
        match self {
            Self::RG(rs) => {
                rs.sf_synthesizer.note_off_all(false);
            }
            Self::CG(cs) => {
               
            }
            _ => {}
        }
    }


    pub fn setInstrument(&mut self, channel:u8, ins:u8) {
        match self {
            Self::RG(rs) => {
                rs.setInstrument(channel, ins);
            }
            Self::CG(cs) => {
                let _ = cs.setInstrument(channel , ins);
            }
            _ => {}
        }
    }
    pub fn sendCC(&mut self, channel:u8, controller:u8, value:u8) {
        match self {
            Self::RG(rs) => {
                rs.sf_synthesizer.process_midi_message(channel.into(), 0xB0,controller.into(),value.into());
            }
            // Self::CG(cs) => {

            // }
            _ => {}
        }
    }

    pub fn pitchBend(&mut self, channel:u8, msb:u8, lsb:u8) {
        match self {
            Self::RG(rs) => {
                rs.sf_synthesizer.process_midi_message(channel.into(), 0xE0,msb.into(),lsb.into());
            }
            // Self::CG(cs) => {

            // }
            _ => {}
        }
    }
}
